Breve explicação 
===

A única função alterada foi CA_TEST, sua declaração está contida na linha 2639 da classe CheapStat_v2.c.

A manipulação da memória é inicializada juntamente com a função principal main(), onde são apagados todos os dados contidos nela. Após isso, chamo a função CA_TEST(LINHA 261) passando os paramentos necessários para seu funcionamento a fim de facilitar o teste, este trecho de código não faz parte do produto final.

Na função CA_TEST, inicializo a escrita no endereço zero - Linha 2642, desta forma a memória está preparada para receber dados e os escrever sequencialmente. Os dados são efetivamente escritos nas linhas 2717, 2722 etc, após passarem por uma validação que determinará se é preciso ou não reiniciar o endereço (isso porque a memória escreve sequencialmente apenas 256 bytes). 

No projeto original, a captura de dados é feita dentro de um loop enquanto o tempo de análise não foi esperado - LINHA 2706 primeira aparição. Neste trecho de código não posso escrever os dados na memória pois o valor de interesse é apenas o último lido. Desta forma, coloquei uma variável que recebe esses dados para que quando o loop for finalizado eu possa escrever o dado na memória - **Aqui está o problema**. Ainda não descobri o motivo, mas a função de escrita não funciona nesta parte do código, funciona no início da função, antes do loop, dentro dele, mas não após ele.