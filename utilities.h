#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <stdint.h>
#include <string.h>
#include "spi_functions.h"
#include "utilities.h"

void convertType(uint16_t value, SPI_Master_t spi);

void baseConverter(int16_t address, SPI_Master_t spi);

void changeType(char *hexValue, SPI_Master_t spi);

void changeAddress(int16_t address, SPI_Master_t spi);


#endif 