

#ifndef SPI_FUNCTIONS_H_
#define SPI_FUNCTIONS_H_

#include "spi_driver.h"
#include "utilities.h"

void enable_write_data(SPI_Master_t spi);

void disable_write_data(SPI_Master_t spi);

void erase_sector(SPI_Master_t spi, uint8_t address);

void erase_block(SPI_Master_t spi, uint8_t address);

void erase_chip(SPI_Master_t spi);

void page_program_data(SPI_Master_t spi, uint8_t MSB, uint8_t middle, uint8_t LSB, uint8_t data);

uint8_t read_data(SPI_Master_t spi, uint8_t address);

void initializeWritingSequential(SPI_Master_t spi);

void writeSequentially(SPI_Master_t spi, uint16_t data, int16_t address, int value);

void finalizeWriteSequential(SPI_Master_t spi);

void readSequentially(SPI_Master_t spi, uint8_t MSB, uint8_t middle, uint8_t LSB, int size);

void startAddress(SPI_Master_t spi);

void disablaSequentialRead(SPI_Master_t spi);

void initializeOnZeroAddress(SPI_Master_t spi);

#endif