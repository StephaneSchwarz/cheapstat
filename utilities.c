#include <stdint.h>
#include <string.h>
#include "spi_functions.h"
#include "utilities.h"


void convertType(uint16_t value, SPI_Master_t spi){

	uint8_t number;

	number = value & 0xff;
	SPI_MasterTransceiveByte(&spi, number);
	
	number = (value >> 8) & 0xff;
	SPI_MasterTransceiveByte(&spi, number);	
}

void baseConverter(int16_t address, SPI_Master_t spi){
	
	char hexValue[6];

	itoa(address, hexValue, 16);
	
	changeType(hexValue, spi);
}

void changeType(char *hexValue, SPI_Master_t spi){

	int i,
	length = strlen(hexValue),
	indInt = 2,
	indChar = 1,
	vetInt[3] = {0};

	char vetChar[2] = {""};

	for (i = (length - 1); i >= 0; i--)	{
		
		vetChar[indChar--] = hexValue[i];

		if (indChar == -1){
			vetInt[indInt--] = strtol(vetChar, NULL, 16);
			indChar = 1;
			memset(vetChar, ' ', 2);
		}
	}
	if (length % 2){
		vetInt[indInt] = strtol(vetChar, NULL, 16);
	}

	SPI_MasterTransceiveByte(&spi, vetInt[0]);
	SPI_MasterTransceiveByte(&spi, vetInt[1]);
	SPI_MasterTransceiveByte(&spi, vetInt[2]);
}

void changeAddress(int16_t address, SPI_Master_t spi){
	
	baseConverter(address, spi);
}