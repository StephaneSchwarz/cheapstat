#include "spi_driver.h"
#include "spi_functions.h"
#include "utilities.h"


void enable_write_data(SPI_Master_t spi){
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0x06); // Opcode to write enable
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight	
}

void disable_write_data(SPI_Master_t spi){
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0x04); // Opcode to write disable
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight	
}

void erase_sector(SPI_Master_t spi, uint8_t address){
	
	enable_write_data(spi);
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0xD7); // Opcode to erase a sector
		
		SPI_MasterTransceiveByte(&spi, address); /* This follow three address   */
		SPI_MasterTransceiveByte(&spi, address); /* bytes are required to erase */
		SPI_MasterTransceiveByte(&spi, address); /* a SECTOR of the memory      */
		
	PORTD.OUTSET = PIN4_bm; // Chip select hight
	
	disable_write_data(spi);	
}

void erase_block(SPI_Master_t spi, uint8_t address){
	
	enable_write_data(spi);
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0xD8); // Opcode to erase a block
		
		SPI_MasterTransceiveByte(&spi, address); /* This follow three address    */
		SPI_MasterTransceiveByte(&spi, address); /* bytes are required to erase  */
		SPI_MasterTransceiveByte(&spi, address); /* a BLOCK of the memory        */
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight
	
	disable_write_data(spi);	
}

void erase_chip(SPI_Master_t spi){
	
	enable_write_data(spi);
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0xC7); // Opcode to erase the chip
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight
	
	disable_write_data(spi);	
}

void page_program_data(SPI_Master_t spi, uint8_t MSB, uint8_t middle, uint8_t LSB, uint8_t data){
	
	enable_write_data(spi);
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0x02); // Opcode to write a data into the memory
		
		SPI_MasterTransceiveByte(&spi, MSB);	 // Most significant bit
		SPI_MasterTransceiveByte(&spi, middle); // "Middle" significant bit
		SPI_MasterTransceiveByte(&spi, LSB);   // Least significant bit
		
		SPI_MasterTransceiveByte(&spi, data);		
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight
	
	disable_write_data(spi);	
}

uint8_t read_data(SPI_Master_t spi, uint8_t address){
	
	uint8_t dataReturned;
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0x03); // Opcode to read data into the memory
	
		SPI_MasterTransceiveByte(&spi, address);	/* This address represents     */
		SPI_MasterTransceiveByte(&spi, 00); 		/* the location data, but only */
		SPI_MasterTransceiveByte(&spi, 00); 		/* the MSB will be decode      */
		
		//dataReturned = SPI_MasterTransceiveByte(&spi, 00); // This is just a clock to output the data memory
		dataReturned = SPI_MasterTransceiveByte(&spi, 00); // This is just a clock to output the data memory
	
	PORTD.OUTSET = PIN4_bm; // Chip select hight
	
	return dataReturned;
}

/*
	This function write only 255 bytes into the memory, after that is necessary 
	write in another memory page. In this function you don't disable the writing
	and don't set the chip select as hight. Thus, when you need to read a data of
	the memory disable the written and set as hight the CS.
*/
void initializeWritingSequential(SPI_Master_t spi){

	enable_write_data(spi);
	
	PORTD.OUTCLR = PIN4_bm; // Chip select low
	
		SPI_MasterTransceiveByte(&spi, 0x02); // Opcode to write a data into the memory
}

/*
	Before the initialization of the write sequential function you can
	use the function. 
*/
void writeSequentially(SPI_Master_t spi, uint16_t data, int16_t address, int value){
	
	/*
	*   address =  0   	-> Just write the data into the memory
	*	address >  0 	-> Change the address value and write the data in the memory
	*/
		
	if (value){
		
		finalizeWriteSequential(spi);
		initializeWritingSequential(spi);
		changeAddress(address, spi);
		convertType(data, spi);
		
	}else{
		
		convertType(data, spi);
	}	
}

/*
	 Before write a data in the memory initialize the 
	 first address value
*/
void initializeOnZeroAddress(SPI_Master_t spi){
	
	initializeWritingSequential(spi);
	startAddress(spi);	
}

/* 
	After write sequential use this function to disable the write mode
	and set as hight the CS.
*/
void finalizeWriteSequential(SPI_Master_t spi){

	PORTD.OUTSET = PIN4_bm;

	disable_write_data(spi);
}

/*  
 	Remember that these values ​​are composed by two bytes, 
	but the micro controller not support process many data,
	thus the better choice is compute this values ​​on the computer.
*/
void readSequentially(SPI_Master_t spi, uint8_t MSB, uint8_t middle, uint8_t LSB, int size){
	
	PORTD.OUTCLR = PIN4_bm;

		SPI_MasterTransceiveByte(&spi, 0x03); // Opcode to read data into the memory
	
		SPI_MasterTransceiveByte(&spi, MSB); 		/* This address represents     */		
		SPI_MasterTransceiveByte(&spi, middle); 	/* the location data, but only */
		SPI_MasterTransceiveByte(&spi, LSB); 		/* the MSB will be decode      */
		
		for(int i = 0; i < size; i++){
			
			SPI_MasterTransceiveByte(&spi, 00);
			
		}
}
/*
	This function just send to the spi three values
	that represent the first address to write data.
*/
void startAddress(SPI_Master_t spi){

	SPI_MasterTransceiveByte(&spi, 00);
	SPI_MasterTransceiveByte(&spi, 00);
	SPI_MasterTransceiveByte(&spi, 00);
	
}

void disablaSequentialRead(SPI_Master_t spi){

	PORTD.OUTSET = PIN4_bm;
}
